# Cold Shot

#### January 25, 2023

<blockquote>
<p>I don’t like it and I’m sorry I ever had anything to do with it.</p>
<p class="author">Erwin Schrödinger</p>
</blockquote>

In reality, cinemagoers may or may not purchase tickets for Alec Baldwin’s new Western, _Rust_. They may or may not visit the concession stand before handing their tickets to the ticket taker, who may or may not then rip each ticket in half before sending patrons down carpet halls to designated theaters, stubs in hand.

_Rust_ audiences may or may not carry morbid curiosity to their seats. They may or may not have followed the production’s early, bad publicity when news of the shooting death of cinematographer Halyna Hutchins spread via media outlets and online platforms. As previews end, house lights darken, and the first frames of the feature presentation appear onscreen, they may or may not keep an eye out for the scene that Baldwin had begun to rehearse before shooting Hutchins from a distance of approximately two feet.

The prodution of _Rust_ may or may not continue after a series of civil lawsuits, naming Hutchins’ widower as one of the producers, alongside Baldwin. They may or may not name a replacement armorer, one with more confidence and experience than Hannah Gutierrez-Reed, the armorer in charge of the gun that killed Hutchins.

The show may or may not go on.

Cinemagoers may or may not enjoy _Rust_. Critics may or may not review it positively, as calculated by the review aggregator site Rotten Tomatoes.

Baldwin and first assistant director David Halls may or may not receive involuntary manslaughter convictions. Prosecutors in court may or may not assert that the domain and rules of the _Rust_ set exist within the domain and laws of real life and the State of New Mexico, and not the other way around.

Baldwin and Halls may or may not have the stomach to read any reviews at all. They may or may not go to prison. Baldwin may or may not wish he’d had his lawyer present when talking to the Santa Fe County Sheriff’s Office, who informed him of Hutchins’ death towards the end of their interrogation. He may or may not choose to make further statements to the press.

I may or may not see _Rust_. I may or may not attempt to suspend disbelief and immerse myself in the universe of the film, to connect with the motifs of the Western mythos — the heroic but broken men, the barren but beautiful landscapes, the forlorn but rugged horses, and the hot but familiar guns.

When David Halls took in his hand the replica Colt .45 single-action revolver that would kill his coworker and announced “cold gun,” he may or may not have known about a pile of beer cans in the nearby desert, many with bullet holes punched through their logos by bored crewmembers who had borrowed that very gun for target practice with live ammunition.

Depending on who you ask, Halls may or may not have held the replica Colt .45 single-action revolver in his hand at all. Depending on who you ask, armorer Gutierrez-Reed may or may not have displayed its open cylinder to Baldwin prior to the shooting. Depending on who you ask, Baldwin may or may not have pulled the trigger. Depending on who you ask, the cast and crew of _Rust_ may or may not have followed established firearms safety protocols, and may or may not have felt safe on set.

When Alec Baldwin pulled the replica Colt .45 single-action revolver from its leather holster and pointed it directly at Hutchins and director Joel Souza, Baldwin may or may not have thought of the gun in his hand as a prop rather than a weapon, and he may or may not have thought of himself as Harland _Rust_ the retired outlaw, rather than Alec Baldwin the actor and producer.

At the moment he said “bang,” and the replica Colt .45 single-action revolver subsequently discharged, he may or may not have noticed the stronger recoil of the barrel, and the more violent register of its report, as compared to that produced by a blank cartridge. He may or may not have detected a hint of sulfur in the air.

As the emergency helicopter carrying Halyna Hutchins lifted off from the set of _Rust_, she may or may not have glimpsed, through bubble-shaped windows, the cluster of sun-greyed wooden buildings that make up the Bonanza Creek Ranch, including the movie church on whose wide-planked floor she had just collapsed, bleeding.

She may or may not have blinked and squinted like a new bride at the dazzling New Mexico sun when paramedics carried her out through the front doors of the movie church. As the helicopter carried her over the ranch, she may or may not have seen the iron cross atop the movie church’s belfry and thought of the spires on Eastern Orthodox churches of her youth, and felt terrified and alone and in pain. She may or may not have seen the yucca, the sagebrush, the horse trails, or the ghost mines in the hills below, before losing consciousness.

At the end of _Rust_, as the closing credits roll and audiences emerge from the world of the movie and begin to stand, stretch, and collect their things, they may or may not notice a dedication to the memory of Halyna Hutchins among the list of actors and crew. They may or may not squint and blink their eyes as they enter the world of the carpet hallway, then the world of the theater lobby, then the world of the sidewalk outside. They may or may not discard their ticket stubs.

When in their dark chambers they sleep that night, they may or may not feel that they really exist.

##### TR
